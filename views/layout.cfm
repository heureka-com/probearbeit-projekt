<!--- Place HTML here that should be used as the default layout of your application. --->
<html>
  <head>
    <cfoutput>
		#styleSheetLinkTag("styles")#
		#csrfMetaTags()#
	</cfoutput>
  </head>

	<body>
		<cfoutput>
		#flashMessages()#
		#includeContent()#
		</cfoutput>
	</body>
</html>
