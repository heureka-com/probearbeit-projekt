<CFOUTPUT>
    #styleSheetLinkTag("start-styles")#
    <div class="start-body">
        #imageTag(source="logo.png", class="logo")#
        <div class = "start-wrapper">
            <h1>Hallo und Willkommen zu deinem Probetag!</h1>
            <div>
                <p>
                    Wir hoffen dass du dich schnell zurecht finden wirst. 
                    Zum Start kannst du dir <a href="https://gitlab.com/heureka-com/probearbeit-projekt/-/boards">hier</a> einen &Uuml;berblick &uuml;ber deine heutigen Aufgaben verschaffen. 
                    Solltest du auf Probleme sto&szlig;en, welche du selbst nicht l&ouml;sen kannst, melde dich gerne bei einem von uns! <br><br>
                    Wir w&uuml;nschen dir viel Erfolg!
                </p>
                
                <h2>Regeln</h2>
                <p>
                    Prinzipiell sind die Regeln sehr einfach: Du darfst alles verwenden.
                    <br>
                    <br>
                    Bedenke hierbei jedoch: Solltest du Code-Beispiele aus dem Internet &uuml;bernehmen, solltest du diese jedoch auch verstehen und erkl&auml;ren k&ouml;nnen.
                    Sp&auml;ter werden wir das Programmierte mit dir besprechen und erwarten hier auch, dass verstanden wurde was geleistet wurde!
                </p>

                <h2>Technologien</h2>
                <p>
                    Das Projekt, in welchem du hier arbeiten wirst, fu&szlig;t auf zwei Technologien (Neben HTML und CSS):
                    <ul>
                        <li><a href="https://medium.com/@worldclasssoftware/what-is-coldfusion-and-cfml-what-are-their-advantages-eea04502f0d4">ColdFusion</a>:
                            Ist die Serversprache, mit welcher die Serverapplikation programmiert wird.
                            Hier bestehen zwei M&ouml;glichkeiten zu programmieren. 
                            Zum einen CFML, was HTML sehr &auml;hnlich sieht und zum anderen CFScript, welches eine Skriptsprache ist.
                        </li>
                        <li><a href="https://cfwheels.org/">CFWheels:</a>
                            Ist ein ColdFusion Framework, welches viele Funktionalit&auml;ten und eine grundlegende Architektur (<a href="https://wiki.selfhtml.org/wiki/MVC">MVC</a>) bereitstellt.
                            F&uuml;r dich ist hierbei vorerst nur wichtig, dass es das Verkn&uuml;pfen zwischen Dateien und das Verkn&uuml;pfen dieser mit URLs (<a href="https://guides.cfwheels.org/v1.4/docs/using-routes">Routing</a>) &uuml;bernimmt.
                        </li>
                    </ul>
                </p>
                    <h2>Hilfestellung</h2>
                    <ul>
                        <li>Verwendung von Icons: 
                            Eine M&ouml;glichkeit ist die Verwendung von <a href="https://fontawesome.com/how-to-use/on-the-web/setup/hosting-font-awesome-yourself">Font Awesome</a>. 
                            Du kannst Font Awesome, wie in der Anleitung beschrieben, einbinden, das gew&uuml;nschte Icon aus der <a href="https://fontawesome.com/icons?d=listing&m=free">Icon Liste</a> ausw&auml;hlen und verwenden.
                        </li>
                        <li>Ideen: Einrichten von Routen, Session Management? Oder Array?, Loops, Post/Get Stuff</li>
                    </ul>

                <h2>N&uuml;tzliches</h2>

                <p>Mit einer Dokumentation kannst du nachschlagen, welchen Zweck eine Funktion hat und wie du sie anwenden kannst.                 
                    Folgend werden die f&uuml;r dieses Projekt wichtigen Dokumentationen aufgez&auml;hlt:</p>
                <ul>
                    <li><a href="https://cfdocs.org/">ColdFusion Dokumentation</a></li>
                    <li><a href="https://api.cfwheels.org/v2.1">CFWheels Dokumentation</a></li>
                </ul>

                <p>Mit dem CFWheels Framework wird eine spezielle Ordner- und Dateistruktur angelegt.
                    Folgend werden n&uuml;tzliche Dateipfade aufgez&auml;hlt:</p>
                <ul>
                    <li><span class="tabbed-list">Projektverzeichnis:</span>    D:\projekte\probearbeit-projekt</li>
                    <li><span class="tabbed-list">Wrapper f&uuml;r alle Views:</span>\views\layout.cfm</li>
                    <li><span class="tabbed-list">Routing:</span>               \config\routes.cfm</li>
                    <li><span class="tabbed-list">Views (Wildcard Routing):</span>\views\<i>controller-name</i>\<i>action-name</i>.cfm</li>
                    <li><span class="tabbed-list">Controller:</span>            \controller\<i>controller-name</i>.cfc</li>
                    <li><span class="tabbed-list">CSS Stylesheet:</span>        \stylesheets\styles.css</li>
                </ul>
            </div>
        </div>
    </div>
</CFOUTPUT>